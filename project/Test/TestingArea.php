<?php

require_once '../Debugger.php';

$debugger = new Debugger('./', Debugger::LEVEL_INFO, Debugger::MODE_APPEND);

$debugger->write('Test', Debugger::LEVEL_INFO);

$test2 = array('Test1 in array', 'Test2 in array');

$debugger->write($test2, Debugger::LEVEL_INFO);

